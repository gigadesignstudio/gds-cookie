<script type="text/javascript">var __namespace = '<?php echo $namespace; ?>';</script>
<h2 class="header">Cookie Notification Bar Setting</h2>

<!-- Display update message if options have been updated -->
<?php if( isset( $_GET['message'] ) ): ?>
  <div id="message" class="updated below-h2">
    <p><?php _e('Options successfully updated!', $namespace); ?></p>
  </div>
<?php endif; ?>

<div class="config-wrap">
  <form action="" method="post" id="<?php echo $namespace; ?>-form">
  <div id="content">
  <?php wp_nonce_field( $namespace . "-update-options" ); ?>
  <div>
    <p>
      Verifica il messaggio che verrà mostrato nella barra.
      Ricordati di cambiare il link alla pagina della cookie policy.
      <textarea style="width: 80%;  padding: 1rem;" rows="5" name="data[message]"><?php echo ($this->get_option( 'message', $_REQUEST['lang'] ) ); ?></textarea>
      <textarea style="width: 80%;  padding: 1rem;" rows="5" name="data[button]"><?php echo ($this->get_option( 'button', $_REQUEST['lang'] ) ); ?></textarea>
    </p>
    <!-- <p>
      Inserisci questo SCSS nel tuo codice e modificalo a tuo piacimento.
<pre style="background: white; width: 80%; padding: 1rem;">
.gds-cookie {
  position: fixed;
  bottom: 0;
  width: 100%;
  background: white;
  padding: 0;
  z-index: 100;
  border-top: 1px solid #333;
}

.gds-cookie__wrapper {
  width: 100%;
  max-width: 120rem;
  margin: 0 auto;
  padding: 1rem 2rem;
  @include box-sizing(border-box);
}

.gds-cookie__text {
  padding-right: 2rem;
}

.gds-cookie__content {
  width: 100%;
  position: relative;
}

.gds-cookie__close {
  display: block;
  position: absolute;
  right: 0;
  top: 50%;
  @include transform(translateY(-50%));
}
</pre>
    </p> -->
  </div>
  <div class="submit">
    <input type="submit" name="Submit" class="button-primary" value="<?php _e( "Save Changes", $namespace ); ?>" />
  </div>
</form>
