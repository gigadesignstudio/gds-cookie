<?php
/*
Plugin Name: Cookie Notification Bar
Plugin URI: https://www.gigadesignstudio.com
Description: Cookie Notification Bar
Version: 1.0
Author: Giga Design Studio
Author URI: https://www.gigadesignstudio.com
*/


class GDS_Cookie {
  var $namespace = "gds-cookie";
  var $friendly_name = "Cookie Settings";
  var $option_name_in_db = "gds-cookie";
  var $version = 1;

 function hooks () {
   add_action( 'init', array($this, 'route'));
   add_action('wp_footer', array($this , 'show_cookie_bar_theme'));
   add_action('admin_menu', array( $this, 'admin_menu' ));
 }

 private function _sanitize( $str ) {
     if ( !function_exists( 'wp_kses' ) ) {
         require_once( ABSPATH . 'wp-includes/kses.php' );
     }
     global $allowedposttags;
     global $allowedprotocols;

     if ( is_string( $str ) ) {
         $str = wp_kses( $str, $allowedposttags, $allowedprotocols );
     } elseif( is_array( $str ) ) {
         $arr = array();
         foreach( (array) $str as $key => $val ) {
             $arr[$key] = $this->_sanitize( $val );
         }
         $str = $arr;
     }

     return $str;
 }

 private function _admin_options_update() {
   if( wp_verify_nonce( $_REQUEST['_wpnonce'], "{$this->namespace}-update-options" ) ) {
      $option_name_in_db = $this->option_name_in_db;
        if ($_REQUEST['lang']) {
          $option_name_in_db .= "_" . $_REQUEST['lang'];
        }
       $data = array();
       foreach( $_POST['data'] as $key => $val ) {
           $data[$key] = $this->_sanitize( $val );
       }
       update_option($option_name_in_db, $data );
       wp_safe_redirect( $_REQUEST['_wp_http_referer'] . '&message=1' );
       exit;
   }
 }

 function route() {
     $uri = $_SERVER['REQUEST_URI'];
     $protocol = isset( $_SERVER['HTTPS'] ) ? 'https' : 'http';
     $hostname = $_SERVER['HTTP_HOST'];
     $url = "{$protocol}://{$hostname}{$uri}";
     $is_post = (bool) ( strtoupper( $_SERVER['REQUEST_METHOD'] ) == "POST" );

     // Check if a nonce was passed in the request
     if( isset( $_REQUEST['_wpnonce'] ) ) {
         $nonce = $_REQUEST['_wpnonce'];

         // Handle POST requests
         if( $is_post ) {
             if( wp_verify_nonce( $nonce, "{$this->namespace}-update-options" ) ) {
                 $this->_admin_options_update();
             }
         }
         // Handle GET requests
         else {

         }
     }
 }

 function admin_menu() {
     $page_hook = add_options_page( $this->friendly_name, $this->friendly_name, 'administrator', $this->namespace, array( $this, 'admin_options_page' ) );
 }

 function get_option( $option_name, $lang = null ) {
    $option_name_in_db = $this->option_name_in_db;
    if ($lang) {
      $option_name_in_db .= "_" . $lang;
    }
   $db_value = maybe_unserialize(get_option($option_name_in_db));
   $db_value_fallback = maybe_unserialize(get_option($this->option_name_in_db));
   if ($db_value[$option_name]) {
     return $db_value[$option_name];
   } else if ($db_value_fallback[$option_name]) {
    return $db_value_fallback[$option_name];
  } else {
     if ($option_name == "message") {
       return 'This website uses cookies: By continuing to browse this site you accept this <a href="#">policy</a>.';
     } elseif ($option_name == "button") {
       return "Ok";
     }
   }
 }

 function admin_options_page() {
     if( !current_user_can( 'manage_options' ) ) {
         wp_die( 'You do not have sufficient permissions to access this page' );
     }
     $page_title = $this->friendly_name . ' Options';
     $namespace = $this->namespace;
     include( "gds-cookie-admin.php" );
 }

 function show_cookie_bar_theme() {
    $lang = null;
    if (defined( 'ICL_LANGUAGE_CODE' ) ) {
      $lang = ICL_LANGUAGE_CODE;
    }
    ?>
  		<div class="gds-cookie" id="gds-cookie">
  			<div class="gds-cookie__wrapper">
          <div class="gds-cookie__text">
            <?php
              echo $this->get_option('message', $lang);
  				  ?>
  				</div>
          <a class="gds-cookie__close">
            <?php
              echo $this->get_option('button', $lang);
            ?>
          </a>
  			</div>
  		</div>
   	<?php
  }

}

// Initiate
$GDS_Cookie = new GDS_Cookie();
$GDS_Cookie->hooks();

function gds_cookie_enqueue_scripts_theme_scripts() {
  wp_enqueue_script( 'jquery-script', plugins_url() . '/gds-cookie/js/jquery.cookie.min.js', array( 'jquery' ), '1', true );
  wp_enqueue_script( 'popup-script', plugins_url() . '/gds-cookie/js/popup.min.js', array( 'jquery' ), '1', true );
}
add_action('wp_enqueue_scripts', 'gds_cookie_enqueue_scripts_theme_scripts');

?>
